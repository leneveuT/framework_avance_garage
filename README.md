# Garage

This application was generated using JHipster 7.8.1, you can find documentation and help at [https://www.jhipster.tech](https://www.jhipster.tech).

## Prérequis 

* Java 11
* NodeJS 16
* NPM 8

## Page

Ajout d'une page Garage affichant le nom d'un garage. Cette page est disponible sur la barre de navigation et nécessite une connexion.

## Connexion

Login : admin
Mot de passe : admin

## Demarrage du projet

```
./npmw install
```

```
./mvnw
```

## Commandes

### jhipster entity

```
jhipster entity <name>
```

Cette commande permet de modifier et de gérer les entités de l'application de façon interactive (relation, champs, ...).


## Mémo

Si liquibase affiche des erreurs de migrations au lancement du projet, executer les commandes suivantes :

```
./mvnw clean
```

```
./mvnw liquibase:clearCheckSums
```




package academy.campus.garage.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Vehicule.
 */
@Entity
@Table(name = "vehicule")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Vehicule implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "model", nullable = false)
    private String model;

    @NotNull
    @Column(name = "licence_plate", nullable = false)
    private String licencePlate;

    @Column(name = "first_registration")
    private LocalDate firstRegistration;

    @ManyToOne
    @JsonIgnoreProperties(value = { "vehicules" }, allowSetters = true)
    private Brand brand;

    @ManyToOne
    @JsonIgnoreProperties(value = { "address", "vehicules" }, allowSetters = true)
    private Customer customer;

    @ManyToOne
    private Garage garage;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Vehicule id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModel() {
        return this.model;
    }

    public Vehicule model(String model) {
        this.setModel(model);
        return this;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getLicencePlate() {
        return this.licencePlate;
    }

    public Vehicule licencePlate(String licencePlate) {
        this.setLicencePlate(licencePlate);
        return this;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public LocalDate getFirstRegistration() {
        return this.firstRegistration;
    }

    public Vehicule firstRegistration(LocalDate firstRegistration) {
        this.setFirstRegistration(firstRegistration);
        return this;
    }

    public void setFirstRegistration(LocalDate firstRegistration) {
        this.firstRegistration = firstRegistration;
    }

    public Brand getBrand() {
        return this.brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Vehicule brand(Brand brand) {
        this.setBrand(brand);
        return this;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Vehicule customer(Customer customer) {
        this.setCustomer(customer);
        return this;
    }

    public Garage getGarage() {
        return this.garage;
    }

    public void setGarage(Garage garage) {
        this.garage = garage;
    }

    public Vehicule garage(Garage garage) {
        this.setGarage(garage);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Vehicule)) {
            return false;
        }
        return id != null && id.equals(((Vehicule) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Vehicule{" +
            "id=" + getId() +
            ", model='" + getModel() + "'" +
            ", licencePlate='" + getLicencePlate() + "'" +
            ", firstRegistration='" + getFirstRegistration() + "'" +
            "}";
    }
}

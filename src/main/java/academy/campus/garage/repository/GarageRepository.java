package academy.campus.garage.repository;

import academy.campus.garage.domain.Garage;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Garage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GarageRepository extends JpaRepository<Garage, Long> {}

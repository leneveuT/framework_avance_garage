import { IVehicule } from 'app/entities/vehicule/vehicule.model';

export interface IBrand {
  id?: number;
  name?: string;
  vehicules?: IVehicule[] | null;
}

export class Brand implements IBrand {
  constructor(public id?: number, public name?: string, public vehicules?: IVehicule[] | null) {}
}

export function getBrandIdentifier(brand: IBrand): number | undefined {
  return brand.id;
}

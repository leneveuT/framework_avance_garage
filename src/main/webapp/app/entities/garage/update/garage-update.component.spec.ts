import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { GarageService } from '../service/garage.service';
import { IGarage, Garage } from '../garage.model';

import { GarageUpdateComponent } from './garage-update.component';

describe('Garage Management Update Component', () => {
  let comp: GarageUpdateComponent;
  let fixture: ComponentFixture<GarageUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let garageService: GarageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [GarageUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(GarageUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(GarageUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    garageService = TestBed.inject(GarageService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const garage: IGarage = { id: 456 };

      activatedRoute.data = of({ garage });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(garage));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Garage>>();
      const garage = { id: 123 };
      jest.spyOn(garageService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ garage });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: garage }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(garageService.update).toHaveBeenCalledWith(garage);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Garage>>();
      const garage = new Garage();
      jest.spyOn(garageService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ garage });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: garage }));
      saveSubject.complete();

      // THEN
      expect(garageService.create).toHaveBeenCalledWith(garage);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Garage>>();
      const garage = { id: 123 };
      jest.spyOn(garageService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ garage });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(garageService.update).toHaveBeenCalledWith(garage);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});

import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IGarage, Garage } from '../garage.model';
import { GarageService } from '../service/garage.service';

@Component({
  selector: 'jhi-garage-update',
  templateUrl: './garage-update.component.html',
})
export class GarageUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
  });

  constructor(protected garageService: GarageService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ garage }) => {
      this.updateForm(garage);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const garage = this.createFromForm();
    if (garage.id !== undefined) {
      this.subscribeToSaveResponse(this.garageService.update(garage));
    } else {
      this.subscribeToSaveResponse(this.garageService.create(garage));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IGarage>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(garage: IGarage): void {
    this.editForm.patchValue({
      id: garage.id,
      name: garage.name,
    });
  }

  protected createFromForm(): IGarage {
    return {
      ...new Garage(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
    };
  }
}

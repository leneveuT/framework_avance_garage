export interface IGarage {
  id?: number;
  name?: string;
}

export class Garage implements IGarage {
  constructor(public id?: number, public name?: string) {}
}

export function getGarageIdentifier(garage: IGarage): number | undefined {
  return garage.id;
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IVehicule, getVehiculeIdentifier } from '../vehicule.model';

export type EntityResponseType = HttpResponse<IVehicule>;
export type EntityArrayResponseType = HttpResponse<IVehicule[]>;

@Injectable({ providedIn: 'root' })
export class VehiculeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/vehicules');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(vehicule: IVehicule): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(vehicule);
    return this.http
      .post<IVehicule>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(vehicule: IVehicule): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(vehicule);
    return this.http
      .put<IVehicule>(`${this.resourceUrl}/${getVehiculeIdentifier(vehicule) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(vehicule: IVehicule): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(vehicule);
    return this.http
      .patch<IVehicule>(`${this.resourceUrl}/${getVehiculeIdentifier(vehicule) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IVehicule>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IVehicule[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addVehiculeToCollectionIfMissing(vehiculeCollection: IVehicule[], ...vehiculesToCheck: (IVehicule | null | undefined)[]): IVehicule[] {
    const vehicules: IVehicule[] = vehiculesToCheck.filter(isPresent);
    if (vehicules.length > 0) {
      const vehiculeCollectionIdentifiers = vehiculeCollection.map(vehiculeItem => getVehiculeIdentifier(vehiculeItem)!);
      const vehiculesToAdd = vehicules.filter(vehiculeItem => {
        const vehiculeIdentifier = getVehiculeIdentifier(vehiculeItem);
        if (vehiculeIdentifier == null || vehiculeCollectionIdentifiers.includes(vehiculeIdentifier)) {
          return false;
        }
        vehiculeCollectionIdentifiers.push(vehiculeIdentifier);
        return true;
      });
      return [...vehiculesToAdd, ...vehiculeCollection];
    }
    return vehiculeCollection;
  }

  protected convertDateFromClient(vehicule: IVehicule): IVehicule {
    return Object.assign({}, vehicule, {
      firstRegistration: vehicule.firstRegistration?.isValid() ? vehicule.firstRegistration.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.firstRegistration = res.body.firstRegistration ? dayjs(res.body.firstRegistration) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((vehicule: IVehicule) => {
        vehicule.firstRegistration = vehicule.firstRegistration ? dayjs(vehicule.firstRegistration) : undefined;
      });
    }
    return res;
  }
}

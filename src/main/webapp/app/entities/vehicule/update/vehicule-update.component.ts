import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IVehicule, Vehicule } from '../vehicule.model';
import { VehiculeService } from '../service/vehicule.service';
import { IBrand } from 'app/entities/brand/brand.model';
import { BrandService } from 'app/entities/brand/service/brand.service';
import { ICustomer } from 'app/entities/customer/customer.model';
import { CustomerService } from 'app/entities/customer/service/customer.service';
import { IGarage } from 'app/entities/garage/garage.model';
import { GarageService } from 'app/entities/garage/service/garage.service';

@Component({
  selector: 'jhi-vehicule-update',
  templateUrl: './vehicule-update.component.html',
})
export class VehiculeUpdateComponent implements OnInit {
  isSaving = false;

  brandsSharedCollection: IBrand[] = [];
  customersSharedCollection: ICustomer[] = [];
  garagesSharedCollection: IGarage[] = [];

  editForm = this.fb.group({
    id: [],
    model: [null, [Validators.required]],
    licencePlate: [null, [Validators.required]],
    firstRegistration: [],
    brand: [],
    customer: [],
    garage: [],
  });

  constructor(
    protected vehiculeService: VehiculeService,
    protected brandService: BrandService,
    protected customerService: CustomerService,
    protected garageService: GarageService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ vehicule }) => {
      this.updateForm(vehicule);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const vehicule = this.createFromForm();
    if (vehicule.id !== undefined) {
      this.subscribeToSaveResponse(this.vehiculeService.update(vehicule));
    } else {
      this.subscribeToSaveResponse(this.vehiculeService.create(vehicule));
    }
  }

  trackBrandById(_index: number, item: IBrand): number {
    return item.id!;
  }

  trackCustomerById(_index: number, item: ICustomer): number {
    return item.id!;
  }

  trackGarageById(_index: number, item: IGarage): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVehicule>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(vehicule: IVehicule): void {
    this.editForm.patchValue({
      id: vehicule.id,
      model: vehicule.model,
      licencePlate: vehicule.licencePlate,
      firstRegistration: vehicule.firstRegistration,
      brand: vehicule.brand,
      customer: vehicule.customer,
      garage: vehicule.garage,
    });

    this.brandsSharedCollection = this.brandService.addBrandToCollectionIfMissing(this.brandsSharedCollection, vehicule.brand);
    this.customersSharedCollection = this.customerService.addCustomerToCollectionIfMissing(
      this.customersSharedCollection,
      vehicule.customer
    );
    this.garagesSharedCollection = this.garageService.addGarageToCollectionIfMissing(this.garagesSharedCollection, vehicule.garage);
  }

  protected loadRelationshipsOptions(): void {
    this.brandService
      .query()
      .pipe(map((res: HttpResponse<IBrand[]>) => res.body ?? []))
      .pipe(map((brands: IBrand[]) => this.brandService.addBrandToCollectionIfMissing(brands, this.editForm.get('brand')!.value)))
      .subscribe((brands: IBrand[]) => (this.brandsSharedCollection = brands));

    this.customerService
      .query()
      .pipe(map((res: HttpResponse<ICustomer[]>) => res.body ?? []))
      .pipe(
        map((customers: ICustomer[]) =>
          this.customerService.addCustomerToCollectionIfMissing(customers, this.editForm.get('customer')!.value)
        )
      )
      .subscribe((customers: ICustomer[]) => (this.customersSharedCollection = customers));

    this.garageService
      .query()
      .pipe(map((res: HttpResponse<IGarage[]>) => res.body ?? []))
      .pipe(map((garages: IGarage[]) => this.garageService.addGarageToCollectionIfMissing(garages, this.editForm.get('garage')!.value)))
      .subscribe((garages: IGarage[]) => (this.garagesSharedCollection = garages));
  }

  protected createFromForm(): IVehicule {
    return {
      ...new Vehicule(),
      id: this.editForm.get(['id'])!.value,
      model: this.editForm.get(['model'])!.value,
      licencePlate: this.editForm.get(['licencePlate'])!.value,
      firstRegistration: this.editForm.get(['firstRegistration'])!.value,
      brand: this.editForm.get(['brand'])!.value,
      customer: this.editForm.get(['customer'])!.value,
      garage: this.editForm.get(['garage'])!.value,
    };
  }
}

import dayjs from 'dayjs/esm';
import { IBrand } from 'app/entities/brand/brand.model';
import { ICustomer } from 'app/entities/customer/customer.model';
import { IGarage } from 'app/entities/garage/garage.model';

export interface IVehicule {
  id?: number;
  model?: string;
  licencePlate?: string;
  firstRegistration?: dayjs.Dayjs | null;
  brand?: IBrand | null;
  customer?: ICustomer | null;
  garage?: IGarage | null;
}

export class Vehicule implements IVehicule {
  constructor(
    public id?: number,
    public model?: string,
    public licencePlate?: string,
    public firstRegistration?: dayjs.Dayjs | null,
    public brand?: IBrand | null,
    public customer?: ICustomer | null,
    public garage?: IGarage | null
  ) {}
}

export function getVehiculeIdentifier(vehicule: IVehicule): number | undefined {
  return vehicule.id;
}

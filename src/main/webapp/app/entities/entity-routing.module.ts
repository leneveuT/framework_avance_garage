import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'brand',
        data: { pageTitle: 'garageApp.brand.home.title' },
        loadChildren: () => import('./brand/brand.module').then(m => m.BrandModule),
      },
      {
        path: 'vehicule',
        data: { pageTitle: 'garageApp.vehicule.home.title' },
        loadChildren: () => import('./vehicule/vehicule.module').then(m => m.VehiculeModule),
      },
      {
        path: 'customer',
        data: { pageTitle: 'garageApp.customer.home.title' },
        loadChildren: () => import('./customer/customer.module').then(m => m.CustomerModule),
      },
      {
        path: 'address',
        data: { pageTitle: 'garageApp.address.home.title' },
        loadChildren: () => import('./address/address.module').then(m => m.AddressModule),
      },
      {
        path: 'garage',
        data: { pageTitle: 'garageApp.garage.home.title' },
        loadChildren: () => import('./garage/garage.module').then(m => m.GarageModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule { }

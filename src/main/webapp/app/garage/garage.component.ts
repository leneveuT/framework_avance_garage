import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { IGarage } from '../entities/garage/garage.model';

@Component({
  selector: 'jhi-garage',
  templateUrl: './garage.component.html',
  styleUrls: ['./garage.component.scss']
})
export class GarageComponent implements OnInit {

  garage?: IGarage;

  constructor(protected http: HttpClient) { }

  ngOnInit(): void {
    // TODO : Utiliser un service à la place d'une requete en dur
    this.http.get<IGarage>("/api/garages/1").subscribe(garage => {
      this.garage = garage;
    });
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GarageComponent } from './garage.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';



@NgModule({
  imports: [CommonModule, SharedModule, RouterModule.forChild([{
    path: '',
    component: GarageComponent,
    data: {
      pageTitle: 'login.title',
    },
  }])],
  declarations: [GarageComponent],
})
export class GarageModule { }

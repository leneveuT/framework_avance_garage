package academy.campus.garage.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import academy.campus.garage.IntegrationTest;
import academy.campus.garage.domain.Garage;
import academy.campus.garage.repository.GarageRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link GarageResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class GarageResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/garages";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private GarageRepository garageRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restGarageMockMvc;

    private Garage garage;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Garage createEntity(EntityManager em) {
        Garage garage = new Garage().name(DEFAULT_NAME);
        return garage;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Garage createUpdatedEntity(EntityManager em) {
        Garage garage = new Garage().name(UPDATED_NAME);
        return garage;
    }

    @BeforeEach
    public void initTest() {
        garage = createEntity(em);
    }

    @Test
    @Transactional
    void createGarage() throws Exception {
        int databaseSizeBeforeCreate = garageRepository.findAll().size();
        // Create the Garage
        restGarageMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(garage)))
            .andExpect(status().isCreated());

        // Validate the Garage in the database
        List<Garage> garageList = garageRepository.findAll();
        assertThat(garageList).hasSize(databaseSizeBeforeCreate + 1);
        Garage testGarage = garageList.get(garageList.size() - 1);
        assertThat(testGarage.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void createGarageWithExistingId() throws Exception {
        // Create the Garage with an existing ID
        garage.setId(1L);

        int databaseSizeBeforeCreate = garageRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restGarageMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(garage)))
            .andExpect(status().isBadRequest());

        // Validate the Garage in the database
        List<Garage> garageList = garageRepository.findAll();
        assertThat(garageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = garageRepository.findAll().size();
        // set the field null
        garage.setName(null);

        // Create the Garage, which fails.

        restGarageMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(garage)))
            .andExpect(status().isBadRequest());

        List<Garage> garageList = garageRepository.findAll();
        assertThat(garageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllGarages() throws Exception {
        // Initialize the database
        garageRepository.saveAndFlush(garage);

        // Get all the garageList
        restGarageMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(garage.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    void getGarage() throws Exception {
        // Initialize the database
        garageRepository.saveAndFlush(garage);

        // Get the garage
        restGarageMockMvc
            .perform(get(ENTITY_API_URL_ID, garage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(garage.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    void getNonExistingGarage() throws Exception {
        // Get the garage
        restGarageMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewGarage() throws Exception {
        // Initialize the database
        garageRepository.saveAndFlush(garage);

        int databaseSizeBeforeUpdate = garageRepository.findAll().size();

        // Update the garage
        Garage updatedGarage = garageRepository.findById(garage.getId()).get();
        // Disconnect from session so that the updates on updatedGarage are not directly saved in db
        em.detach(updatedGarage);
        updatedGarage.name(UPDATED_NAME);

        restGarageMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedGarage.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedGarage))
            )
            .andExpect(status().isOk());

        // Validate the Garage in the database
        List<Garage> garageList = garageRepository.findAll();
        assertThat(garageList).hasSize(databaseSizeBeforeUpdate);
        Garage testGarage = garageList.get(garageList.size() - 1);
        assertThat(testGarage.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void putNonExistingGarage() throws Exception {
        int databaseSizeBeforeUpdate = garageRepository.findAll().size();
        garage.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGarageMockMvc
            .perform(
                put(ENTITY_API_URL_ID, garage.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(garage))
            )
            .andExpect(status().isBadRequest());

        // Validate the Garage in the database
        List<Garage> garageList = garageRepository.findAll();
        assertThat(garageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchGarage() throws Exception {
        int databaseSizeBeforeUpdate = garageRepository.findAll().size();
        garage.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGarageMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(garage))
            )
            .andExpect(status().isBadRequest());

        // Validate the Garage in the database
        List<Garage> garageList = garageRepository.findAll();
        assertThat(garageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamGarage() throws Exception {
        int databaseSizeBeforeUpdate = garageRepository.findAll().size();
        garage.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGarageMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(garage)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Garage in the database
        List<Garage> garageList = garageRepository.findAll();
        assertThat(garageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateGarageWithPatch() throws Exception {
        // Initialize the database
        garageRepository.saveAndFlush(garage);

        int databaseSizeBeforeUpdate = garageRepository.findAll().size();

        // Update the garage using partial update
        Garage partialUpdatedGarage = new Garage();
        partialUpdatedGarage.setId(garage.getId());

        restGarageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGarage.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGarage))
            )
            .andExpect(status().isOk());

        // Validate the Garage in the database
        List<Garage> garageList = garageRepository.findAll();
        assertThat(garageList).hasSize(databaseSizeBeforeUpdate);
        Garage testGarage = garageList.get(garageList.size() - 1);
        assertThat(testGarage.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void fullUpdateGarageWithPatch() throws Exception {
        // Initialize the database
        garageRepository.saveAndFlush(garage);

        int databaseSizeBeforeUpdate = garageRepository.findAll().size();

        // Update the garage using partial update
        Garage partialUpdatedGarage = new Garage();
        partialUpdatedGarage.setId(garage.getId());

        partialUpdatedGarage.name(UPDATED_NAME);

        restGarageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGarage.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGarage))
            )
            .andExpect(status().isOk());

        // Validate the Garage in the database
        List<Garage> garageList = garageRepository.findAll();
        assertThat(garageList).hasSize(databaseSizeBeforeUpdate);
        Garage testGarage = garageList.get(garageList.size() - 1);
        assertThat(testGarage.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void patchNonExistingGarage() throws Exception {
        int databaseSizeBeforeUpdate = garageRepository.findAll().size();
        garage.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGarageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, garage.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(garage))
            )
            .andExpect(status().isBadRequest());

        // Validate the Garage in the database
        List<Garage> garageList = garageRepository.findAll();
        assertThat(garageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchGarage() throws Exception {
        int databaseSizeBeforeUpdate = garageRepository.findAll().size();
        garage.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGarageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(garage))
            )
            .andExpect(status().isBadRequest());

        // Validate the Garage in the database
        List<Garage> garageList = garageRepository.findAll();
        assertThat(garageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamGarage() throws Exception {
        int databaseSizeBeforeUpdate = garageRepository.findAll().size();
        garage.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGarageMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(garage)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Garage in the database
        List<Garage> garageList = garageRepository.findAll();
        assertThat(garageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteGarage() throws Exception {
        // Initialize the database
        garageRepository.saveAndFlush(garage);

        int databaseSizeBeforeDelete = garageRepository.findAll().size();

        // Delete the garage
        restGarageMockMvc
            .perform(delete(ENTITY_API_URL_ID, garage.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Garage> garageList = garageRepository.findAll();
        assertThat(garageList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
